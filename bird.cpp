///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file bird.cpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author Gavin Onizuka <gonizuka@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   23_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "bird.hpp"

using namespace std;

namespace animalfarm {

void Bird::printInfo() {
   Animal::printInfo();
   cout << "   Feather Color = [" << colorName( featherColor ) << "]" << endl;
   cout << "   Migratory = [" << boolalpha << isMigratory << "]" << endl;
}

}
